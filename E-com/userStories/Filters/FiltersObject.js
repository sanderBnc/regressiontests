import { ClientFunction, Selector, t } from 'testcafe';

const getWindowLocation = ClientFunction(() => window.location);

export default class FiltersObject {
    constructor () {
        this.productCount = 999999;
        this.resultsCountSelector = Selector('.productoverview__searchResults').find('p > b');
        this.catArray = [];
        this.catObject = [];
        
    }

    async getCats(){
        let x = await Selector('.filter-category > .caret');
        await t.expect(x.innerText).notEql('');
        //console.log(await x.count);
        this.catArray = [];
        for (let i = 0; i < await x.count ; i++){
            this.catArray.push((await x.nth(i).innerText).trim());
        }
        for (let  i= 0;i < this.catArray.length;i++){
            this.catObject[i] = [];
        }
    }


    async clickSpecFilter(filterCat, filterName){
        //get initial values
        //console.log(this.catObject);
        let filterValue = await Selector('.filter-value').withText(filterName);
        let checkbox = await filterValue.find('input');
        let checked = await checkbox.checked;

        let currentIndex = this.catArray.indexOf(filterCat);
        if (!checked){
            await this.catObject[currentIndex].push(filterName);
        } else if (checked){
            await this.catObject[currentIndex].pop(filterName);
        }
        //console.log(this.catObject);
        //console.log(this.catArray);
        //console.log('checked: ' + await checkbox.checked);

        await t.expect(this.resultsCountSelector.innerText).notEql('');
        this.productCount = parseInt(await this.resultsCountSelector.innerText);
        await t.click(filterValue.find('label'));
        let resultsCount = parseInt(await this.resultsCountSelector.innerText);

        await t.expect(!checked).eql(await checkbox.checked); 
        //if it was checked assert >= results else if unchecked assert <= results
        await this.checkSpecs(resultsCount, checkbox, currentIndex);
        // let location = await getWindowLocation();   
        // DEZE AANZETTEN ALS HET PRODUCT WERKT await this.checkSpecOnLink(filterName, location);
    }

    async checkSpecs(resultsCount, checkbox, currentIndex){
        let selected = await checkbox.checked;
        //special promotie
        if (currentIndex == 0) {
            //aangevinkt
            if (selected){
                await t
                    .expect(resultsCount).lte(this.productCount);
            //onaangevinkt
            } else if(!selected){
                await t
                    .expect(resultsCount).gte(this.productCount); 
            } else {
                throw 'helemaal kapot';
            }
        } else if (currentIndex == 1){
            //console.log('test price here');
        } else {
            //aangevinkt
            //1e
            if (selected && this.catObject[currentIndex].length <= 1){
                await t
                    .expect(resultsCount).lte(this.productCount);
            //2e of meer
            } else if(selected && this.catObject[currentIndex].length > 1){
                await t
                    .expect(resultsCount).gte(this.productCount);
            //onaangevinkt
            //nog 1 of meer over
            } else if (!selected && this.catObject[currentIndex].length > 0){
                await t
                    .expect(resultsCount).lte(this.productCount);
            //niks meer over
            } else if (!selected && this.catObject[currentIndex].length == 0){
                await t
                    .expect(resultsCount).gte(this.productCount);
            } else {
                throw 'helemaal kapot2';
            }
        }
        //console.log(await checkbox.checked);
        //console.log(this.productCount);
        //console.log(resultsCount);
        //console.log('--------');
    }

    async checkSpec(checked, resultsCount, checkbox){
        if (checked){
            await t
                .expect(resultsCount).lte(this.productCount);
        } else if(!checked){
            await t
                .expect(resultsCount).gte(this.productCount); 
        } else {
            throw 'helemaal kapot';
        }
        await t.expect(!checked).eql(await checkbox.checked); 
    }

  
    async checkSpecOnLink(filterName, location){
        await t
            .click(Selector('.product__img'));
           
        let specs = await Selector('.product__specsContainer').innerText + 
                    await Selector('.product__specsContainer').nth(1).innerText +
                    await Selector('.product__specsContainer').nth(2).innerText;
        //console.log(specs);
        await t
            .expect(specs).contains(filterName);
        await t
            .navigateTo(location.href);
    }

    async deleteFilters(fc, filterName, fc2, filterName2){
        await this.getCats();
        await t
            .expect(Selector('.productoverview__deleteFilters').exists).notOk();
        await this.clickSpecFilter(fc, filterName);
        await t
            .expect(Selector('.productoverview__deleteFilters').exists).ok()
            .click(Selector('.productoverview__deleteFilters'))
            .expect(Selector('.productoverview__deleteFilters').exists).notOk();
        
        await this.getCats();
        await this.clickSpecFilter(fc, filterName);
        await t
            .expect(Selector('.activeFilter').withText(filterName).exists).ok()
            .click(Selector('.activeFilter').withText(filterName))
            .expect(Selector('.activeFilter').withText(filterName).exists).notOk()
            .expect(Selector('.productoverview__deleteFilters').exists).notOk();
        
        await this.getCats();
        await this.clickSpecFilter(fc, filterName);
        await this.clickSpecFilter(fc2, filterName2);
        await t
        .expect(Selector('.activeFilter').withText(filterName).exists).ok()
        .expect(Selector('.activeFilter').withText(filterName2).exists).ok()
        .click(Selector('.activeFilter').withText(filterName))
        .click(Selector('.activeFilter').withText(filterName2))
        .expect(Selector('.activeFilter').withText(filterName).exists).notOk()
        .expect(Selector('.activeFilter').withText(filterName2).exists).notOk()
        .expect(Selector('.productoverview__deleteFilters').exists).notOk(); 
    }
}