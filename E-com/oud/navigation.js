import { Selector, ClientFunction, t} from 'testcafe';

fixture.skip `Navigation`
    .page `https://test-winkel.bnc.nl/#/`;

const getWindowLocation = ClientFunction(() => window.location);

class Navigation {
    constructor () {
        this.navBar = this.initNavbar();
    }

    initNavbar(){
        let navBarNames = ['Werkplekken', 'Servers & Opslag', 'Spraak & Data', 'Beeld & Geluid', 'Printers & Scanners']
        let navBarSelectors = [];
        for (const name of navBarNames) {
            navBarSelectors[name] = Selector('.nav-item').withExactText(name);
        }
        return navBarSelectors;
    }

    async verifyNavigation(){
        let location = '';
        for (var key in this.navBar) {
            
            await t
                .click(this.navBar[key]);
            location = await getWindowLocation();

            await t
                .expect(location.href).eql('https://test-winkel.bnc.nl/#/cat/' + key.split(' ')[0].toLowerCase());
        }
    }
}

const navigation = new Navigation();

test('Navigation', async t => {
   await navigation.verifyNavigation();
})
