import { Selector, t } from 'testcafe';

class PageNumbers {
    constructor(){
        this.nextButton = Selector('button').withAttribute('click.delegate', 'goToPage(currentPage+1)');
        this.previousButton = Selector('button').withAttribute('click.delegate', 'goToPage(currentPage-1)');
    }

    async next(){
        await t.click(this.nextButton);
    }

    async previous(){
        await t.click(this.previousButton);
    }
}

export default class SortObject {
    constructor () {
        this.selectSort =  Selector('select').withAttribute('value.bind', 'chosenSortOption');
        this.pages = Selector('.productoverview__searchResults').find('p > b');
        this.PageNumbersPOM = new PageNumbers();
    }

    async clickSelectSort(option){
        await t.click(this.selectSort);
        let currentOption = Selector('option').withText(option);
        await t.click(currentOption);
        this.pages = Selector('.productoverview__searchResults').find('p > b');
        //console.log(this.pages.innerText);
        this.pages = Math.ceil(parseInt(await this.pages.innerText)/12);   
        //console.log(this.pages);
    }

    async testSortedBy(PbcProperty, parseNames){    
        let listNames = [];  
        for (let i = 1; i <= this.pages; i++){
            let pbcCount = await Selector('product-box-component').count;
            //keeps pbcCount cached? so...
            if(i===this.pages){
                let pbcCount2 = await Selector('product-box-component').count;
                pbcCount = pbcCount2;
            }
            //console.log(pbcCount);
            await t.expect(pbcCount).gt(5); //assert count > 5 (4 aanbevolen + 2 om te vergelijken)
            let pbc = Selector('product-box-component').find(PbcProperty);
            //console.log(i);
            if(parseNames == 'name'){
                for(let j = 0; j < pbcCount-4; j++){
                    let pbcPropText = await pbc.nth(j).innerText;
                    listNames.push(pbcPropText);
                }
            } else if(parseNames == 'number' || parseNames == 'rNumber'){
                for(let j = 0; j < pbcCount-4; j++){
                    let pbcPropText = await pbc.nth(j).innerText;
                    pbcPropText = parseFloat(pbcPropText.replace('€ ', '').replace('.', '').replace('-', '0').replace(',', '.'));
                    //console.log(pbcPropText);
                    listNames.push(pbcPropText);
                }
            } else {
                for(let j = pbcCount-5; j >= 0 ; j--){
                    let pbcPropText = await pbc.nth(j).innerText;
                    listNames.push(pbcPropText);
                }
            }
            this.PageNumbersPOM.next();
        }
        //listNames = this.shuffle(listNames);
        //console.log(listNames);
        await t.expect(this.isSorted(listNames)).ok();
    }


    shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    isSorted(listNames, parseNames) {
        let sorted = [];
        for (let i = 0;i<listNames.length ;i++){
            sorted.push(listNames[i]);
        }
        if(parseNames == 'name'){
            sorted.sort();
        } else if(parseNames == 'price'){
            sorted.sort(sortNumber);
        } else if(parseNames == 'rPrice'){
            sorted.sort(sortNumber).reverse();
        }
        
        //console.log(listNames);
        //console.log(sorted);
        for (let i = 0;i<listNames.length ;i++){
            //console.log(listNames[i]);
            //console.log(sorted[i]);
            if(listNames[i] !== sorted[i]){
                return false;
            }
        }
        return true;
    }
}