import { ClientFunction, Selector, t } from 'testcafe';

const getWindowLocation = ClientFunction(() => window.location);

class Item {
    constructor(name, amount){
        this.name = name.toLowerCase().replace(/\n/g,'');
        this.amount = parseInt(amount);
    }
}

export default class WinkelwagenObject {
    constructor () {
        this.items = [  new Item('Playstation 4', 7),
                        new Item('Laptop Maginfique 2000', 2),
                        new Item('Acer Aspire EX 21', 1),
                        new Item('Lenovo Test123', 4),
                        new Item('Macbook Pro 3 miljard inch', 7),
                        new Item('combinatie:playstation 4ipad proipod nano', 2)];
    }

    async addBundle(nthNumber, amount){
        // GET TITLE COMBINATIE: X   
        let bundle = await Selector('.bundleProduct').nth(nthNumber);
        let bundleItems = await bundle.find('.bundleProduct__product');
        let bundleItemsCount = await bundleItems.count;


        await t.typeText(bundle.find('.product__quantityInput'), amount.toString(), { replace: true });
        let name = 'combinatie:';
        for (let i = 0; i < await bundleItemsCount; i++){
            let title = bundleItems.nth(i).find('.product__title');
            name += await title.innerText;
            let quantity = await bundleItems.nth(i).find('.bundleCount');
            // UNCOMMENT IF WINKELMANDJE HAS BUNDLECOUNT
            // if (await quantity.exists){
            //     await t.expect(await quantity.innerText).notEql('');
            //     console.log(await quantity.innerText);
            //     name += '(' + await quantity.innerText + ')';
            // }
        }
        //add if new item or add to other products
        this.addItemToObject(name, amount);

        //click add
        await t.click(bundle.find('.product__plusButton'));

        //check if added correctly
        await this.checkBasketPopup();
    }

    async addItemNr(nthNumber, amount){
        //select nth product
        let product = await Selector('product-box-component').nth(nthNumber);
        
        //set name and amount
        let name = await product.find('.pbcTitle').innerText;
        await t.typeText(product.find('.product__quantityInput'), amount.toString(), { replace: true });

        //add if new item or add to other products
        this.addItemToObject(name, amount);

        //click add
        await t.click(product.find('.product__plusButton'));

        //check if added correctly
        await this.checkBasketPopup();
    }

    addItemToObject(name, amount){
        let flagNew = true;
        for (var item in this.items) {
            if(this.items[item].name === name.toLowerCase()){
                this.items[item].amount += amount;
                flagNew = false;
            }
        }
        if(flagNew) {
            this.items.push(new Item(name, amount));
        }
    }

    deleteItemFromObject(name){
        var array = [];
        for (var i in this.items){
            array.push(this.items[i].name);
        }
        const index = array.indexOf(name.toLowerCase());
        if (index !== -1) {
            this.items.splice(index, 1);
        }
    }
    
    async deleteItem(nthNumber){
        //select nth product
        let product = await Selector('.cart__item').nth(nthNumber);

        //get name for check
        let name = await product.find('.cart__itemTitle').innerText;

        //delete item
        this.deleteItemFromObject(name);

        //click delete
        await t.click(product.find('div').withAttribute('click.delegate', 'deleteItem(item)'));

        //check if added correctly
        await this.checkBasketPopup();
    }

    async deleteItemPopup(nthNumber){
        //select nth product
        let product = await Selector('.orderLine').nth(nthNumber);

        //get name for check
        let name = await product.find('.item--title').innerText;

        //delete item
        this.deleteItemFromObject(name);

        //click delete
        await t.click(product.find('div').withAttribute('click.delegate', 'deleteItem(item)'));

        //check if added correctly
        await this.checkBasketPopup();
    }

    async checkBasketPopup(){
        //change back to click
        await t.hover(Selector('#cart-icon-div'));
        let itemsSelector = await Selector('.orderLineWrapper').find('.orderLine');
        let itemsPopup = [];
        for (let i = 0; i < await itemsSelector.count; i++){
            let title = await itemsSelector.nth(i).find('.item--title').innerText;
            let amount = await itemsSelector.nth(i).find('input').withAttribute('value.bind', 'item.quantity & validate').value;
            itemsPopup.push(new Item(title, amount));
        }
        //console.log(itemsPopup);
        //console.log(this.items);
        await t.expect(itemsPopup).eql(this.items);
        //change to unclick popup.
        await t.hover(Selector('.grey-top-border'));
    }
}

