//Als webshopgebruiker wil ik producten kunnen sorteren op prijs, artikelcode, producttitel en andere eigenschappen.
import { ClientFunction } from 'testcafe';
import VergelijkObject from './vergelijkObject';

const getWindowLocation = ClientFunction(() => window.location);
//const vergelijkPOM = new VergelijkObject();

fixture `Product Vergelijken`
    .page `https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops`


test.skip('Product via detailpagina toevoegen', async t => {
    //
});


 //make vergelijkpom WITH NAME OF CAT
 //don't add the same twice
test('Product via overzichtpagina toevoegen', async t => {
    let vergelijkLaptops = new VergelijkObject('Laptop');
    await t.expect(vergelijkLaptops.vergelijkSelector.exists).notOk();
    await vergelijkLaptops.addProduct('overzicht', 1, t);
    await vergelijkLaptops.addProduct('overzicht', 5, t);
});

test('Max 3 toevoegen', async t => {
    let vergelijkLaptops = new VergelijkObject('Laptop');
    await vergelijkLaptops.addProduct('overzicht', 0, t);
    await vergelijkLaptops.addProduct('overzicht', 5, t);
    await vergelijkLaptops.addProduct('overzicht', 7, t);
    await vergelijkLaptops.addProduct('overzicht', 11, t);
});

test('Product verwijderen', async t => {
    let vergelijkLaptops = new VergelijkObject('Laptop');
    await vergelijkLaptops.addProduct('overzicht', 0, t);
    await vergelijkLaptops.addProduct('overzicht', 5, t);
    await t.expect(vergelijkLaptops.vergelijkSelector.exists).ok();
    
    await vergelijkLaptops.deleteProduct(1, t);
    await vergelijkLaptops.deleteProduct(0, t);
    await t.expect(vergelijkLaptops.vergelijkSelector.exists).notOk();
});

test('Product blijft bestaan met navigeren', async t => {
    let vergelijkLaptops = new VergelijkObject('Laptop');
    await vergelijkLaptops.addProduct('overzicht', 7, t);
    await vergelijkLaptops.addProduct('overzicht', 6, t);

    await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Muizen');
    await t.expect(vergelijkLaptops.vergelijkSelector.exists).notOk();

    let vergelijkMuizen = new VergelijkObject('Muizen');
    await vergelijkMuizen.addProduct('overzicht', 1, t);
    await vergelijkMuizen.addProduct('overzicht', 4, t);
    await t.expect(vergelijkMuizen.vergelijkSelector.exists).ok();

    await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops');
    await t.expect(vergelijkLaptops.vergelijkSelector.exists).ok();
    await vergelijkLaptops.addProduct('overzicht', 8, t);
    await vergelijkLaptops.deleteProduct(0, t);
});




