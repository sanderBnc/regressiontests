import { ClientFunction, Selector } from 'testcafe';
import Filters from './FiltersObject';


const getWindowLocation = ClientFunction(() => window.location);

fixture `Product Filteren`
    .page `https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops`
    .beforeEach( async t => {
        let location = await getWindowLocation();   
        await t
            .expect(location.href).eql('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops');
    })
    .afterEach( async t => {
        let location = await getWindowLocation();   
        await t
            .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/cat\/werkplekken\/Laptops)/);
    });


test('select 1 spec', async t => {
    //setup
    const filters = new Filters();
    await filters.getCats(); 

    await filters.clickSpecFilter('schermdiagonaal', '13 inch');
});

test('Select + unselect a spec', async t => {
    //setup
    const filters = new Filters();
    await filters.getCats(); 

    await filters.clickSpecFilter('schermdiagonaal', '16 inch');
    await filters.clickSpecFilter('schermdiagonaal', '16 inch');
});

test('2 specs same category, then unselect 1 spec', async t => {
    //setup
    const filters = new Filters();
    await filters.getCats(); 

    await filters.clickSpecFilter('schermdiagonaal', '16 inch');
    await filters.clickSpecFilter('speciale promotie', 'sale');
    await filters.clickSpecFilter('schermdiagonaal', '16 inch');
});


test('select 1 spec + 1 promotie, then unselect the spec', async t => {
    //setup
    const filters = new Filters();
    await filters.getCats(); 

    await filters.clickSpecFilter('schermdiagonaal', '13 inch');
    await filters.clickSpecFilter('speciale promotie', 'van Anco');
    await filters.clickSpecFilter('schermdiagonaal', '13 inch');
});

test('select 1 spec + 1 promotie, then unselect the promotie', async t => {
    //setup
    const filters = new Filters();
    await filters.getCats(); 

    await filters.clickSpecFilter('speciale promotie', 'sale');
    await filters.clickSpecFilter('schermdiagonaal', '13 inch');
    await filters.clickSpecFilter('speciale promotie', 'sale');
});

test('select filter multiple categories and unselecting some of them', async t => {
    //setup
    const filters = new Filters();
    await filters.getCats(); 

    await filters.clickSpecFilter('schermdiagonaal', '13 inch');
    await filters.clickSpecFilter('schermdiagonaal', '13 inch');
    await filters.clickSpecFilter('speciale promotie', 'sale');
    await filters.clickSpecFilter('speciale promotie', 'van Anco');
    await filters.clickSpecFilter('speciale promotie', 'sale');
    await filters.clickSpecFilter('kloksnelheid', '1600 MHz');
    await filters.clickSpecFilter('processor', 'Intel Celeron');
    await filters.clickSpecFilter('processor', 'Intel Core i7');
    await filters.clickSpecFilter('processor', 'Intel i5 iCore');
    await filters.clickSpecFilter('processor', 'Intel Celeron');
    await filters.clickSpecFilter('processor', 'Intel Core i7');
    await filters.clickSpecFilter('processor', 'Intel i5 iCore');
});

test('Verwijderen 1,2 filters', async t => {
    //setup
    const filters = new Filters();

    await filters.deleteFilters('schermdiagonaal', '16 inch', 'processor', 'Intel i5 iCore');
});
