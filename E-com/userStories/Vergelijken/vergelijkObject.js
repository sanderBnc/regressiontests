import { Selector, t } from 'testcafe';


export default class VergelijkObject {
    constructor(vergelijkCategorie) {
        this.vergelijkCategorie = vergelijkCategorie;
        this.vergelijkSelector = Selector('#productsComparator');
        this.vergelijkList = [];
    }

    async addProduct(ozOfDetail, nthProduct, t) {
        let productSelector = Selector('product-box-component').nth(nthProduct);
        let vergelijkButton = productSelector.find('label').withExactText('Vergelijk');
        await t.click(vergelijkButton);
        let title = await productSelector.find('.pbcTitle').innerText;
        if (this.vergelijkList.length < 3){
            this.vergelijkList.push(title);
            //console.log('title: ' +  title);
            //console.log(await this.vergelijkList);
            await t.expect(this.vergelijkSelector.innerText).contains(title);
        } else {
            await t.expect(this.vergelijkSelector.innerText).notContains(title);
        }
        
    }

    async deleteProduct(nthProduct, t) {
        let productSelector = this.vergelijkSelector.find('div').withAttribute('class', 'productCard').nth(nthProduct);
        //console.log(await productSelector.innerText);
        let deleteButton = productSelector.child('.removeItemIcon');
        //console.log(await deleteButton.innerText);
        await t.click(deleteButton);
        let title = this.vergelijkList[nthProduct];
        //console.log('title: ' +  title);
        //console.log(await this.vergelijkList);
        if (this.vergelijkList.length > 1) {
            this.vergelijkList.pop(title);
            //console.log(await this.vergelijkSelector.innerText+'///');
            //console.log(await Selector('#productsComparator').innerText+'---');
            await t.expect(this.vergelijkSelector.innerText).notContains(title);
            //console.log('title: ' +  title);
            //console.log(await this.vergelijkList);
        } else if (this.vergelijkList.length == 1) {
            this.vergelijkList.pop(title);
            await t.expect(this.vergelijkSelector.exists).notOk();
            //console.log('title: ' +  title);
            //console.log(await this.vergelijkList);
        } else {
            await t.expect(this.vergelijkSelector.innerText).contains(title);
        }
        
    }

}