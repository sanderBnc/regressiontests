import { Selector, ClientFunction } from 'testcafe';

const getWindowLocation = ClientFunction(() => window.location);

fixture.skip `ProductBoxLandingspagina` 
.page `https://test-winkel.bnc.nl/#/`;


test('Klik aanbevolen product', async t => {    
    let location = await getWindowLocation();
    const imageDiv = Selector('.product__img');
        
    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/)/)
        .expect(imageDiv.exists).ok()
        .expect(imageDiv.find('img').exists).ok()
        .expect(imageDiv.find('img').getAttribute('src')).notEql('')
        .click(imageDiv);
    location = await getWindowLocation();
    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/cat\/werkplekken\/)\w+(\/)\d+/g);
});


fixture.skip `ProductBoxDetailpagina` 
.page `https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/545`;

test('Voeg 1 product toe aan winkelmandje', async t => {
    const addButton = await Selector('.product__plusButton');
    let cartCount = await Selector('.cartCount').innerText;
    cartCount = (parseInt(cartCount) + 1).toString();
    const shoppingCartButton = Selector('.shoppingBasket');
    const itemTitle = await Selector('.pbcTitle').innerText;
    const navBarTop = await Selector('#navBar').find('.line-height-65');

    await t
        .click(shoppingCartButton)
        .expect(Selector('.orderLineWrapper').innerText).notContains(itemTitle)
        .click(navBarTop)
        .click(addButton)
        .expect(Selector('.orderLineWrapper').innerText).contains(itemTitle)
        .expect(Selector('.cartCount').innerText.toLowerCase()).eql(cartCount.toLowerCase())
        .click(shoppingCartButton);  

    var itemQuantity = await Selector('.orderLine').withText(itemTitle).find('input');
    await t
        .expect(Selector('.orderLineWrapper').innerText).contains(itemTitle)
        .expect(itemQuantity.value).eql('1')
        .click('.icons__link-to-cart-container');
    let location = await getWindowLocation();
    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/cart)/); 
});


