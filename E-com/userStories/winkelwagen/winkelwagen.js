import { Selector } from 'testcafe';
import WinkelwagenObject from './WinkelwagenObject';


fixture `Winkelwagen` 
    .page `https://test-winkel.bnc.nl/#/cart`;


test('Add from overzichtpagina', async t => {
    const Winkelwagen = new WinkelwagenObject();

    await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops');
    await t.expect(Selector('product-box-component').count).gte(4);   
    await Winkelwagen.addItemNr(3, 5);
    await Winkelwagen.addItemNr(3, 5);
    await Winkelwagen.addItemNr(4, 2);

})

test('Add from detailpagina', async t => {
    const Winkelwagen = new WinkelwagenObject();

    await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/564');
    await t.expect(Selector('product-box-component').count).gte(4);   
    await Winkelwagen.addItemNr(0, 2);
    await Winkelwagen.addItemNr(0, 2);

    // add other detail page items

    // await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/555');
    // await t.expect(Selector('product-box-component').count).gte(4);   
    // await Winkelwagen.addItemNr(3, 5);
    
    // await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Muizen/51');
    // await t.expect(Selector('product-box-component').count).gte(4);   
    // await Winkelwagen.addItemNr(3, 5);

})

test('Add combivoordeel detailpagina', async t => {
    const Winkelwagen = new WinkelwagenObject();

    await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/612');
    await t.expect(Selector('.bundleProduct').count).gte(1);
    await Winkelwagen.addBundle(0, 3);
})


test('Delete from winkelwagen popup', async t => {
    const Winkelwagen = new WinkelwagenObject();

    await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops');
    await t.expect(Selector('product-box-component').count).gte(4);   
    await Winkelwagen.addItemNr(2, 2); 
    await Winkelwagen.addItemNr(6, 4);
  
    //without undo 
    await t.navigateTo('https://test-winkel.bnc.nl/#/');
    await t.hover(Selector('#cart-icon-div'));
    await Winkelwagen.deleteItemPopup(3); 

    //with undo
})

test('Delete from winkelwagen page', async t => {
    const Winkelwagen = new WinkelwagenObject();

    await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops');    
    await t.expect(Selector('product-box-component').count).gte(4);   
    await Winkelwagen.addItemNr(2, 2); 
    await Winkelwagen.addItemNr(6, 4);
 
    //without undo
    await t.navigateTo('https://test-winkel.bnc.nl/#/cart');
    await Winkelwagen.deleteItem(2);

    //with undo
})

